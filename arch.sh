#!/bin/bash

################## GETS SUDO PASSWORD ###############################

passwords(){

local pass=$(mktemp 2>/dev/null)

trap "rm -f $pass" 0 1 2 5 15


# dialog --title "Sudo Password" --clear --insecure --passwordbox "Password:" 10 10 --stdout 2>pass
# pass=$?
#
# password=$pass
dialog --title "Sudo Password" --clear --insecure --passwordbox "Password:" 10 10 2>$pass

password=$(<"${pass}")




}



################ ABOUT COMPUTER SECTION #####################

neoFetch(){

if [[ -f "/usr/bin/neofetch" ]]; then
        dialog --clear
        clear
        neofetch
        read -p "Press enter to continue"
        main
        exit

    else
        dialog --clear
        dialog --title "neofetch Not Installed" \
        dialog --msgbox "\n You do not have neofetch installed" 10 20
        dialog --title "Install?" --yesno "Want to install neofetch?" 6 20
            response=$?

            case $response in
                0) passwords && sudo -S pacman -S neofetch  && clear && neofetch && read -p "Press enter to continue" && main;;
                1) main;;
                255) echo "Something went wrong"; break;;
            esac
    fi

}

dialogInstaller() {

### Dialog Installer ##############
if [[ ! -f "/usr/bin/dialog" ]]; then
  clear
  echo "You do not have 'Dialog' installed"
  read -p "Would you like to install Dialog? y/n: " yes_to_dialog
    if [ "$yes_to_dialog" == "y" ]; then
      sudo pacman -S dialog
    else
      exit 0;


    fi
fi

return;
}
####################### Updates Your Computer ###################



Update_computer(){

#path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

#cd $path



########## completely updates and removes old packages ###########
dialog --clear --yesno "Do you want to update your computer?" 10 30
update=$?

case $update in

    0) passwords && echo $password | sudo -S pacman -Syu && aurman -Syu;;
    1) clear && exit 0;;
    255) echo "something went wrong";;
esac

kernelUninstall

}


######### Uninstall old kernels #################
kernelUninstall(){
  clear
  dialog --backtitle "Not supported yet..." --infobox "Kernel Uninstaller" 10 50
  sleep 5
  main
}
# kernelUninstall(){
# 	dialog --title "Kernel Uninstall" --defaultno --yesno "Do you want to see if you can uninstall old kernels?" 10 30
#     kernel=$?
#
#
#
#
#
#
# 		case $kernel in
#        0) kern=$(uname -r)
# 		dialog --infobox "!DO NOT DELETE THIS IF MOST CURRENT! \nCurrent Kernel: $kern" 10 50
#         sleep 10
#        kernel=$(dpkg --list | grep linux-image)
# 		 dialog --title "Copy just linux-image-X.X.X-XX-generic" --backtitle "Kernel to uninstall" --infobox "$kernel" 15 70
#         sleep 20;;
#
#
#         1) clear && exit 0;;
#            esac
# local deleteKern=$(tempfile 2>/dev/null)
#
# dialog --title "CTRL + SHIFT + V for the kernel you want to delete i.e linux-image-X.X.X-XX-generic" --backtitle "Select Kernel to Uninstall" --defaultno --inputbox "Kernel: " 15 70 2>$deleteKern
# kernel=$?
# dk=$(<"${deleteKern}")
# clear
#         case $kernel in
# 		0) if [ -d "/usr/src/xpad-0.4" ]; then
# 					sudo apt purge $dk && cd /usr/src/xpad-0.4 && sudo git fetch && sudo git checkout origin/master && sudo dkms remove -m xpad -v 0.4 --all && sudo dkms install -m xpad -v 0.4 && cd $path && sudo update-grub
# 					printf '\e[8;24;80t'
#             if [ -f "/usr/bin/lsterminal" ]; then
#                 if [ -d "/usr/src/xpad-0.4" ]; then
# 					sudo apt purge $dk && cd /usr/src/xpad-0.4 && sudo git fetch && sudo git checkout origin/master && sudo dkms remove -m xpad -v 0.4 --all && sudo dkms install -m xpad -v 0.4 && cd $path && sudo update-grub
# 					lxterminal --geometry=80x24 -e main
#
#                 else
# 					sudo apt purge $dk && sudo update-grub
# 					lxterminal --geometry=80x24 -e main
#
#                 fi
#             fi
#
#
#
# 		else
# 					sudo apt purge $dk && sudo update-grub
# 					printf '\e[8;24;80t'
# 		fi;;
#         1) exit 0;;
#         255) echo "something went wrong"; break;;
#         esac
# }

######## Exit ##########


Exit(){

    dialog --clear
    clear
    exit 0;
}



main() {
clear
dialogInstaller

  INPUT='/tmp/menu.output'
  # numbers are Height x Width x Menu Height

  dialog \
  --backtitle "Update_Computer_GUI v1.0.0 BETA" \
  --nocancel --menu "Please choose one option UP/DOWN arrow keys to select then ENTER to proceed:" 15 55 4 \
  1 "About Computer" \
  2 "Update Computer" \
  3 "Exit" 2>"${INPUT}"
#3 "Fix dpkg 'lock'" \


  menuitem=$(<"${INPUT}")


  case $menuitem in
      1) neoFetch;;
      2) Update_computer;;
      3) Exit;;
  esac
# sleep 10

exit 0;


}
main
